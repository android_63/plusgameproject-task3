package buu.example.plusgameprojecttask3

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private val QUESTION_ACTIVITY_CODE = 0
    private var trueAll = 0
    private var falseAll = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnAdd = findViewById<Button>(R.id.btnAdd)
        val btnDel = findViewById<Button>(R.id.btnDel)
        val btnMul =findViewById<Button>(R.id.btnMul)

        val txtTrMa = findViewById<TextView>(R.id.txtTrMa)
        val txtFaMa = findViewById<TextView>(R.id.txtFaMa)

        txtTrMa.text = "$trueAll"
        txtFaMa.text = "$falseAll"

        btnAdd.setOnClickListener{
            val intent = Intent(MainActivity@this, addActivity::class.java)
            startActivityForResult(intent,QUESTION_ACTIVITY_CODE)
        }
        btnDel.setOnClickListener{
            val intent = Intent(MainActivity@this, delActivity::class.java)
            startActivityForResult(intent,QUESTION_ACTIVITY_CODE)
        }
        btnMul.setOnClickListener{
            val intent = Intent(MainActivity@this, mulActivity::class.java)
            startActivityForResult(intent,QUESTION_ACTIVITY_CODE)
        }
    }

    override fun onActivityResult (requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == QUESTION_ACTIVITY_CODE){
            if(resultCode == Activity.RESULT_OK){
                val returnTrue = data!!.getIntExtra("true",trueAll)
                val returnFalse = data!!.getIntExtra("false",falseAll)
                trueAll += returnTrue
                falseAll += returnFalse
                txtTrMa.text = "$trueAll"
                txtFaMa.text = "$falseAll"
            }
        }
    }
}